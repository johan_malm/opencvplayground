//
//  BoxDetectOpenCV.cpp
//  openCvTest
//
//  Created by Johan Haglund Malm on 27/02/16.
//  Copyright © 2016 Johan Haglund Malm. All rights reserved.
//

#include "BoxDetectOpenCV.hpp"
#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace std;
using namespace cv;

#define CAMERA_OUTPUT_WINDOW_NAME "camera-output"

static void find_squares(Mat& image, vector<vector<Point> >& squares);
static cv::Mat debugSquares( std::vector<std::vector<cv::Point> > squares, cv::Mat image );
static double angle( cv::Point pt1, cv::Point pt2, cv::Point pt0 );



static double angle( Point pt1, Point pt2, Point pt0 ) {
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1 * dx2 + dy1 * dy2) / sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10);
}

static Mat debugSquares( vector<vector<Point> > squares, Mat image ) {
    
    for ( int i = 0; i< squares.size(); i++ ) {
        // draw contour
        
        //cout << "i is:" << i << endl;
        //double area = fabs(contourArea(Mat(squares[i])));
        //cout << "area is: " << area << endl;
        
        drawContours(image, squares, i, cv::Scalar(0,255,0), 2, 8, std::vector<cv::Vec4i>(), 0, cv::Point());
        
        // draw bounding rect
        //cv::Rect rect = boundingRect(cv::Mat(squares[i]));
        //cv::rectangle(image, rect.tl(), rect.br(), cv::Scalar(255,0,0), 2, 8, 0);
        
        // draw rotated rect
        /*
         cv::RotatedRect minRect = minAreaRect(cv::Mat(squares[i]));
         cv::Point2f rect_points[4];
         minRect.points( rect_points );
         for ( int j = 0; j < 4; j++ ) {
         cv::line( image,
         rect_points[j],
         rect_points[(j+1) % 4],
         cv::Scalar(0,0,255),
         5,
         8
         ); // blue
         }
         */
    }
    return image;
}

static void find_squares(Mat& image, vector<vector<Point> >& squares) {
    
    squares.clear();
    
    // blur will enhance edge detection
    //Mat blurred(image);
    Mat blurred = image.clone();
    medianBlur(image, blurred, 9);
    
    Mat gray0(blurred.size(), CV_8U);
    Mat gray;
    
    vector<vector<Point> > contours;
    
    // find squares in every color plane of the image
    for (int c = 0; c < 3; c++) {
        
        int ch[] = {c, 0};
        mixChannels(&blurred, 1, &gray0, 1, ch, 1);
        
        if (c == 2) {
            imshow("MixChannels", gray0);
        }
        // try several threshold levels
        //const int threshold_level = 2;
        const int threshold_level = 2;
        for (int l = 0; l < threshold_level; l++) {
            
            // Use Canny instead of zero threshold level!
            // Canny helps to catch squares with gradient shading
            if (l == 0) {
                Canny(gray0, gray, 20, 50, 3); //
                
                // Dilate helps to remove potential holes between edge segments
                dilate(gray, gray, Mat(), Point(-1, -1));
                imshow("GrayCany", gray);
            } else {
                gray = gray0 >= (l+1) * 255 / threshold_level;
                imshow("GrayTres", gray);
            }
            
            // Find contours and store them in a list
            findContours(gray, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
            
            // Test contours
            vector<Point> approx;
            for (size_t i = 0; i < contours.size(); i++) {
                // approximate contour with accuracy proportional
                // to the contour perimeter
                double epsilon = arcLength(Mat(contours[i]), true) * 0.02;
                approxPolyDP(Mat(contours[i]), approx, epsilon, true);
                
                // Note: absolute value of an area is used because
                // area may be positive or negative - in accordance with the
                // contour orientation
                
                if (approx.size() == 4 &&
                    fabs(contourArea(Mat(approx))) > 1000 &&
                    isContourConvex(Mat(approx)))
                {
                    //cout << "approx: " << approx << "\n" << endl;
                    //cout << "Countour area: " << fabs(contourArea(Mat(approx))) << endl;
                    double maxCosine = 0;
                    
                    for (int j = 2; j < 5; j++) {
                        double cosine = fabs(angle(approx[j % 4], approx[j - 2], approx[j - 1]));
                        maxCosine = MAX(maxCosine, cosine);
                    }
                    if (maxCosine < 0.3) {
                        squares.push_back(approx);
                    }
                }
            }
        }
    }
}

cv::Point2f computeIntersect(cv::Vec4i a,
                             cv::Vec4i b)
{
    int x1 = a[0], y1 = a[1], x2 = a[2], y2 = a[3], x3 = b[0], y3 = b[1], x4 = b[2], y4 = b[3];
    
    if (float d = ((float)(x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4)))
    {
        cv::Point2f pt;
        pt.x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / d;
        pt.y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / d;
        return pt;
    }
    else
        return cv::Point2f(-1, -1);
}

static void findSquaresHought(Mat& image, vector<vector<Point> >& squares) {
    
    Mat processImage = image.clone();
    Mat canyImage;
    
    int threshhold1 = 100;
    int threshhold2 = 100;
    int apertureSize = 3;
    
    cv::cvtColor(processImage, canyImage, CV_BGR2GRAY);
    cv::blur(canyImage, canyImage, cv::Size(3, 3));
    Canny(canyImage, canyImage, threshhold1, threshhold2, apertureSize);
    
    imshow("HoughtTransform", canyImage);
    
    std::vector<cv::Vec4i> lines;
    double rho = 1;
    double theta = CV_PI / 180;
    int threshold = 70;
    double minLineLength = 30;
    double maxLineGap = 10;
    //int
    HoughLinesP(canyImage, lines, rho, theta, threshold, minLineLength, maxLineGap);
    
    Mat dst = image.clone();
    // Draw lines
    for (int i = 0; i < lines.size(); i++) {
        cv::Vec4i v = lines[i];
        cv::line(dst, cv::Point(v[0], v[1]), cv::Point(v[2], v[3]), CV_RGB(0,255,0), 3);
    }
    
    imshow("Lines", dst);
}


//: MARK Public

void PaperDetectorTest(const char * argv[]) {
    /// Load the source image
    
    Mat inputImage = imread( argv[1], 1 );
    Size size(500, 500);
    Mat imageResized;
    resize(inputImage, imageResized, size);
    
    imshow("Unprocessed Image",imageResized);
    
    vector<vector<Point>> squareGrid;
}

void sortSquare( vector<Point> paperCordinatesInputOutput) {
    Point2f middle;
    for (int i = 0; i < 4; i++) {
        middle.x += paperCordinatesInputOutput[i].x;
        middle.y += paperCordinatesInputOutput[i].y;
    }
    middle.x = middle.x / (paperCordinatesInputOutput.size());
    middle. y = middle.y / (paperCordinatesInputOutput.size());
    cout << "Middle x: " << middle.x << endl;
    cout << "Middle y: " << middle.y << endl;
    
    vector<cv::Point2i> top;
    vector<cv::Point2i> bot;
    for (int i = 0; i < paperCordinatesInputOutput.size(); i++) {
        if (paperCordinatesInputOutput[i].y < middle.y)
            top.push_back(paperCordinatesInputOutput[i]);
        else
            bot.push_back(paperCordinatesInputOutput[i]);
    }
    cv::Point2f tl = top[0].x > top[1].x ? top[1] : top[0];
    cv::Point2f tr = top[0].x > top[1].x ? top[0] : top[1];
    cv::Point2f bl = bot[0].x > bot[1].x ? bot[1] : bot[0];
    cv::Point2f br = bot[0].x > bot[1].x ? bot[0] : bot[1];
    
    paperCordinatesInputOutput.clear();
    paperCordinatesInputOutput.push_back(tl);
    paperCordinatesInputOutput.push_back(tr);
    paperCordinatesInputOutput.push_back(br);
    paperCordinatesInputOutput.push_back(bl);
    
}


int PaperDetectorCAM(void) {
    VideoCapture cam(0);
    if(cam.isOpened() == false) {
        return -1;
    }
    
    int windowSizeY = 250;
    int windowSizeX = windowSizeY * 16 / 10;
    
    namedWindow("Final",  WINDOW_FREERATIO);
    moveWindow("Final", 0, 100);
    
    namedWindow("MixChannels", WINDOW_FREERATIO);
    moveWindow("MixChannels", windowSizeX, 100);
    
    namedWindow("GrayCany",  WINDOW_FREERATIO);
    moveWindow("GrayCany", 0, windowSizeY + 130);
    
    namedWindow("GrayTres", WINDOW_FREERATIO);
    moveWindow("GrayTres", windowSizeX, windowSizeY + 130);
    
    namedWindow("HoughtTransform", WINDOW_FREERATIO);
    moveWindow("HoughtTransform", 2 * windowSizeX, 100);
    
    namedWindow("Lines", WINDOW_FREERATIO);
    moveWindow("Lines", 2 * windowSizeX, windowSizeY + 130);
    
    namedWindow("PaperTransformed", WINDOW_FREERATIO);
    moveWindow("PaperTransformed", windowSizeX * 2, 100);
    
    Mat edges;
    while (true) {
        
        Mat frame;
        cam >> frame; // get a new frame from camera
        Mat resizeFrame;
        
        
        Size size(windowSizeX, windowSizeY);
        resize(frame, resizeFrame, size);
        
        vector<vector<Point>> squareGrid;
        
        find_squares(resizeFrame, squareGrid);
        debugSquares(squareGrid, resizeFrame);
        
        //squareGrid.clear();
        //findSquaresHought(resizeFrame, squareGrid);
        
        //cvtColor(frame, edges, COLOR_BGR2GRAY);
        //GaussianBlur(edges, edges, Size(7,7), 1.5, 1.5);
        //Canny(edges, edges, 0, 30, 3);
        
        imshow("Final", resizeFrame);
        
        if (squareGrid.size() > 0) {
            vector<Point> corners = squareGrid[0];
            cout <<"Unsorted square: " << corners << endl;
            sortSquare(corners);
            cout <<"Sorted Square: " << corners << endl;
            
            vector<Point2f> cornersFloat;
            for (int i = 0; i < 4; i++) {
                Point2f point(corners[i].x, corners[i].y);
                cornersFloat.push_back(point);
            }
            // ----
            // Define the destination image
            cv::Mat quad = cv::Mat::zeros(600, 423, CV_8UC3);
            
            // Corners of the destination image
            std::vector<cv::Point2f> quad_pts;
            quad_pts.push_back(cv::Point2f(0, 0));
            quad_pts.push_back(cv::Point2f(quad.cols, 0));
            quad_pts.push_back(cv::Point2f(quad.cols, quad.rows));
            quad_pts.push_back(cv::Point2f(0, quad.rows));
            
            // Get transformation matrix
            cv::Mat transmtx = cv::getPerspectiveTransform(cornersFloat, quad_pts);
            
            // Apply perspective transformation
            cv::warpPerspective(resizeFrame, quad, transmtx, quad.size());
            
            //warped = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
            //warped = threshold_adaptive(warped, 250, offset = 10)
            //warped = warped.astype("uint8") * 255
            
            //cvtColor(quad, quad, COLOR_BGR2GRAY);
            //threshold
            //adaptiveThreshold(quad, quad, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 5, 2);
            Mat sharpen;
            cv::GaussianBlur(quad, sharpen, cv::Size(0, 0), 3);
            cv::addWeighted(quad, 1.5, sharpen, -0.5, 0, sharpen);
            
            
            cv::imshow("PaperTransformed", sharpen);
        }
        
        
        // ---
        
        if (waitKey(10) >= 0) {
            cam.release();
            while (cin.get() != '\n') {
                //wait
            }
        };
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}