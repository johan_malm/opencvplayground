//
//  main.cpp
//  openCvTest
//
//  Created by Johan Haglund Malm on 24/02/16.
//  Copyright © 2016 Johan Haglund Malm. All rights reserved.
//

#include <iostream>
#include <opencv2/opencv.hpp>
#include "BoxDetectOpenCV.hpp"

using namespace std;
using namespace cv;

int main(int argc, const char * argv[]) {
    
    cout << "Hello, World!\n" CV_VERSION << endl;
    
    PaperDetectorCAM();
    //PaperDetectorTest(argv);
    
    
    //waitKey();
    
    return 0;
}

