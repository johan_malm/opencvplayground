//
//  BoxDetectOpenCV.hpp
//  openCvTest
//
//  Created by Johan Haglund Malm on 27/02/16.
//  Copyright © 2016 Johan Haglund Malm. All rights reserved.
//

#ifndef BoxDetectOpenCV_hpp
#define BoxDetectOpenCV_hpp

#include <stdio.h>


/// Paper detector
int PaperDetectorCAM(void);
void PaperDetectorTest(const char * argv[]);



#endif /* BoxDetectOpenCV_hpp */
